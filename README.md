# Limitations of on-premises deployment
* The on-premises deployment only supports API v3: https://docs.pdfgeneratorapi.com/v3/

# Requirements
* Linux server, preferably Ubuntu. 
* MySQL version 8.0.+
* A Docker Hub account

# Installation
A token with access to actualreports/pdfgeneratorapi repositoy will be provided to you.

## Install Docker and docker-compose.

Check the links below for instructions.
**Docker:** https://docs.docker.com/engine/install/ubuntu/
**Docker-compose:** https://docs.docker.com/compose/install/

Log into Docker using the access token.  

```docker login --username aronprem```

Pull the pdfgeneratorapi:<version_number> image from Docker

```docker pull actualreports/pdfgeneratorapi:<version_number>```

Version numbers can be found from the tags section: https://bitbucket.org/actualreportsdev/onprem/downloads/?tab=tags

## Configuration

### Application configuration

Configure your environment variables in `pdfgeneratorapi/env` file. 

#### Main configuration

- `MYSQL_HOST=db` - Hostname of your mySQL database
- `MYSQL_PORT=3306` - MySQL database port
- `CACHE_REDIS_HOSTNAME=cache` - Redis host IP or domain
- `SESSION_REDIS_HOSTNAME=cache` - Redis host IP or domain
- `MYSQL_USERNAME=root` - MySQL database username
- `MYSQL_PASSWORD=password` - MySQL database password
  - To use SSL in MySQL add the keys:
    - `MYSQL_SSL_VERIFY_PEER=true` - Enable SSL verification
    - `MYSQL_SSL_KEY=/etc/ssl/certs/client-key.pem` - Path to client key
    - `MYSQL_SSL_CERT=/etc/ssl/certs/client-cert.pem` - Path to client certificate
    - `MYSQL_SSL_CA=/etc/ssl/certs/ca.pem` - Path to CA certificate
- `APP_DOMAIN=http://pdfgeneratorapi.example.com` - App URL with protocol (http or https)
- `SESSION_SECURE=false` - set to `true` in production when using SSL
- `SESSION_SAME_SITE=Lax` - set to `None` to display editor in an iframe (requires `SESSION_SECURE=true`)
- `LOG_ROUTE=stream` - available options are `file` and `stream`. If `file` is used then logs are saved to protected/runtime
- `APP_EMAIL=info@example.com` - the email address used to send out emails from the app (e.g. password reset and team invite emails)

**NB: MySQL username and password should be changed!**

#### Editor theme and layout configuration
It is possible to change the editor layout and menu items using the following environment variables.

- `APP_THEME=` - available only if you have ordered a custom theme.
- `APP_LOGO=` - a URL to publicly accessible image that is shown on the top left corner.
- `EDITOR_COMPONENTS=label,table,composite,chart,barcode,qrcode,image,header,footer,rectangle,vline,hline,pagenumber,symbol,checkbox,radio,signature` - show/hide items in component sidebar
- `EDITOR_MENUS=file,edit,view,insert,help` - show/hide menu items in the editor

#### Email configuration
Emails are sent to users on team invite and password recovery.
It ss possible to use 3 different providers to send emails:
- [Mandrill](https://mailchimp.com)
- [Sendgrid](https://sendgrid.com/)
- SMTP

In order to send emails is required to set the following configuration in your environment variables in pdfgeneratorapi/env file.

- `EMAIL_PROVIDER=` - email provider, avaialble options `mandrill`, `sendgrid` and `smtp`
- `EMAIL_API_USER=` **for sendgrid only**
- `EMAIL_API_KEY=` **for mandrill only**
- `EMAIL_SUBACCOUNT=my_sub_account` - optional, valid only for mandrill
- `EMAIL_SENDER=no-reply@example.com` - email that will be set as sender/reply to
- `EMAIL_SMTP_DSN=smtp://user:pass@smtp.example.com:25` -  for SMTP only, replace the example value with your credentials

#### Document storage configuration
If you want to use `output=url` option then you need to configure an AWS S3 or compatible service to store the generated documents.
If storage is not configured then documents are generated in runtime and removed from memory after it is returned via API.

- `S3_KEY=` - AWS key
- `S3_BUCKET=` - AWS S3 bucket name
- `S3_REGION=` - AWS S3 bucket region
- `S3_DIRECTORY=` - AWS S3 directory in the bucket (optional)

### PHP configuration
If php.ini or php-fpm configuration values need to be overridden use  `php.ini` and `www.conf` files to make the changes needed. 

### REDIS configuration
Create a directory for Redis. By default the Redis directory is `${PWD}/redis`. This can be changed in the `pdfgeneratorapi/docker-compose.yaml` file (line 42).

## Initialization
To start the PDF Generator API run the docker-compose command in the `pdfgeneratorapi` directory.

```docker-compose up -d```

Check if the containers are up and running with the command: `docker ps`

### Generate signature key
Exec into the php-cmd container and get token.

```docker exec php-cmd php yiic generatesecurekey```

Put the token into the `pdfgeneratorapi/env` environment file as `SIGNATURE_KEY` value and restart PHP

```docker restart php-fpm```

### Database
### Database in Docker

Use the same MySQL username and password you configured earlier and update the environment variables in the `db/docker-compose.yaml` file.
Create a directory for MySQL. By default the MySQL directory is ${PWD}/mysql. This can be changed in the `db/docker-compose.yaml` file (line 18).
To start the database run the docker-compose command in the db directory.

```docker-compose up -d```

Check if the container is up and running with the command: `docker ps`

### Database not in Docker

Create a MySQL database with the username and password you configured before in the PDF Generator API environment. Database name should match the database name in the environment file. 

### Database initialization
Exec into the php-cmd container and run an interactive setup script. This script will also create an initial administrator account.

```
docker exec -it php-cmd bash
tools/setup.sh
```

### Run database migrations 
New versions can contain database changes, and you should run migrations after each new version.

```
docker exec php-cmd php yiic migrate
```

You can check applied migrations using following command.

```
docker exec php-cmd php yiic migrate history
```

### Rollback database migrations
It is also possible to rollback applied migrations.

- `docker exec php-cmd php yiic migrate down` - Reverts the last applied migration.
- `docker exec php-cmd php yiic migrate down 3` - Reverts the last 3 applied migrations.


### Database in Docker Desktop on Windows
Remove volumes configuration in `./db/docker-compose.yaml`.

## Done
After that you should be able to access PDF Generator API at the APP_DOMAIN URL.

# Debugging
If needed container logs can be accessed using: `docker logs <container_name>`

Containers can be restarted using: `docker restart <container_name>`


# Kubernetes Deploy

**Prerequisite: Database is located in RDS or similar database service.**

First create a secret for Docker registry access.

`kubectl create secret docker-registry repo-secret --docker-username=onprem@actualreports.com --docker-password=<ACCESS_TOKEN>`

Check https://bitbucket.org/actualreportsdev/onprem/src/master/kubernetes/ repository for Kubernetes yaml examples.

If php.ini or php-fpm configuration values need to be overridden use `php-ini-configmap.yaml` and `php-fpm-configmap.yaml` ConfigMap files to make the changes needed.

Create a ConfigMaps `configmap.yaml`, `nginx-conf.yaml`, `php-ini-configmap.yaml` and `php-fpm-configmap.yaml`.

**NB! Make sure your environment values are correct.** 

Create a Secret for MySQL database password. If you are using `secret.yaml` example the value should be **base64 encoded**. 

Create Deployment. **NB! Check that the Secret and ConfigMap names match the ones you created earlier!**

# Health check
You can use path `/editor/healthcheck` for health check requests. 
You need to whitelist IPs that can make the request using the `HEALTH_CHECK_IPS` environment variable which is a comma separated list of IPs.
You also need to provide `healthcheck` as a header value.
```
curl --location 'http://pdfgeneratorapi.test/editor/healthcheck' --header 'healthcheck;'
```